//motor_control  pin6 , pin7
//sensor blue pin2(inbox) , white pin3(outbox)
//segmen Pin 10 (LOAD /CS) , Pin 11(DIN) , Pin 13(CLK)
// relay fan pin 4
// sensor switch  pin A0

#include <SPI.h>
#include "DHT.h"

#define DHTTYPE DHT11
#define DHTTYPE2 DHT11
#define DHT11PIN 2
#define DHT11PIN2 3

DHT dht(DHT11PIN, DHTTYPE);
DHT dht2(DHT11PIN2, DHTTYPE2);

float h = 0;
float t = 0;
float h2 = 0;
float t2 = 0;
int checkopen = 0;

const int  CS_pin = 10;      
const byte numOfDevices = 2; 

const byte NoOp        = 0x00;
const byte Digit0      = 0x01;
const byte Digit1      = 0x02;
const byte Digit2      = 0x03;
const byte Digit3      = 0x04;
const byte Digit4      = 0x05;
const byte Digit5      = 0x06;
const byte Digit6      = 0x07;
const byte Digit7      = 0x08;
const byte DecodeMode  = 0x09;
const byte Intensity   = 0x0A;
const byte ScanLimit   = 0x0B;
const byte ShutDown    = 0x0C;
const byte DisplayTest = 0x0F;


void SetShutDown(byte Mode) { 
  SetData(ShutDown, !Mode); 
}
void SetScanLimit(byte Digits) { 
  SetData(ScanLimit, Digits); 
}
void SetIntensity(byte intense) { 
  SetData(Intensity, intense); 
}
void SetDecodeMode(byte Mode) { 
  SetData(DecodeMode, Mode); 
}

void SetData(byte adr, byte data) { 
  SetData(adr, data, 255); 
} 

void SetData(byte adr, byte data, byte device)
{
  digitalWrite(CS_pin, LOW);
  for (byte i = numOfDevices; i > 0; i--)
  {
    if ((i == device) || (device == 255))
    {
      SPI.transfer(adr);
      SPI.transfer(data);
    }
    else 
    {
      SPI.transfer(NoOp);
      SPI.transfer(0);
    }
  }
  digitalWrite(CS_pin, HIGH);
  delay(1);
}

void setup()
{
  Serial.begin(9600);
  SPI.begin();

  dht.begin();
  dht2.begin();

  pinMode(4, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(A0, INPUT);

  pinMode(CS_pin, OUTPUT);

  SetDecodeMode(false);
  SetScanLimit(7);
  SetIntensity(5);
  SetShutDown(false);

  digitalWrite(4,HIGH);
  digitalWrite(6,LOW);
  digitalWrite(7,LOW);


  printseg(11,0);
  printseg(11,1);
  printseg(11,2);
  printseg(11,3);
  printseg(11,4);
  printseg(11,5);
  printseg(11,6);
  printseg(11,7);

  delay(1000);
  digitalWrite(6,LOW);
  digitalWrite(7,HIGH);
  delay(1000);
  while(1){
    if(digitalRead(A0) == HIGH){
      break;
    }
    digitalWrite(6,HIGH);
    digitalWrite(7,LOW); 
  }
}
void loop()
{   
  digitalWrite(6,LOW);
  digitalWrite(7,LOW);
  h = dht.readHumidity();
  t = dht.readTemperature(); 
  h2 = dht2.readHumidity();
  t2 = dht2.readTemperature();
  showsegandserial();

  if(h >= h2){
    if (checkopen == 0){
      digitalWrite(4,LOW);  
      digitalWrite(6,LOW);
      digitalWrite(7,HIGH);   
      delay(4000);
      checkopen = 1;
    }

  }
  else if(h < h2){
    while(1){
      digitalWrite(4,LOW);  
      if(digitalRead(A0) == HIGH){
        digitalWrite(4,HIGH);
        checkopen = 0;
        break;
      }
      digitalWrite(6,HIGH);
      digitalWrite(7,LOW); 
    }
  }
}

void showsegandserial(){
  int a,b,c,d,e,f,g,x;  
  Serial.print("T = ");  
  Serial.print(t);
  Serial.print(" *c \t");
  Serial.print("H = ");  
  Serial.print(h);
  Serial.print("% \t");                
  Serial.print("\n");
  Serial.print("T2 = ");  
  Serial.print(t2);
  Serial.print(" *c \t");
  Serial.print("H2 = ");  
  Serial.print(h2);
  Serial.print("% \t");                
  Serial.print("\n");

  a = h2/10;
  b = h2-(a*10);
  c = t2/10;
  d = t2-(c*10);
  printseg(a,3);
  printseg(b,2);
  printseg(c,1);
  printseg(d,0);

  e = h/10;
  f = h-(e*10);
  g = t/10;
  x = t-(g*10);
  printseg(e,7);
  printseg(f,6);
  printseg(g,5);
  printseg(x,4);
}

void printseg(int num , int digit){
  if(digit == 0){
    if(num == 0){
      SetData(Digit0, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit0, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit0, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit0, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit0, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit0, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit0, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit0, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit0, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit0, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit0,0b00000000, 1); 
    }
  }
  if(digit == 1){
    if(num == 0){
      SetData(Digit1, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit1, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit1, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit1, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit1, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit1, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit1, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit1, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit1, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit1, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit1,0b00000000, 1); 
    }
  }
  if(digit == 2){
    if(num == 0){
      SetData(Digit2, 0b11111110, 1); 
    }
    if(num == 1){
      SetData(Digit2, 0b10110000, 1); 
    }
    if(num == 2){
      SetData(Digit2, 0b11101101, 1); 
    }
    if(num == 3){
      SetData(Digit2, 0b11111001, 1); 
    }
    if(num == 4){
      SetData(Digit2, 0b10110011, 1); 
    }
    if(num == 5){
      SetData(Digit2, 0b11011011, 1); 
    }
    if(num == 6){
      SetData(Digit2, 0b11011111, 1); 
    }
    if(num == 7){
      SetData(Digit2, 0b11110000, 1); 
    }
    if(num == 8){
      SetData(Digit2, 0b11111111, 1); 
    }
    if(num == 9){
      SetData(Digit2, 0b11111011, 1); 
    }
    if(num == 11){
      SetData(Digit2,0b00000000, 1); 
    }
  }
  if(digit == 3){
    if(num == 0){
      SetData(Digit3, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit3, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit3, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit3, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit3, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit3, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit3, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit3, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit3, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit3, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit3,0b00000000, 1); 
    }
  }
  if(digit == 4){
    if(num == 0){
      SetData(Digit4, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit4, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit4, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit4, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit4, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit4, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit4, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit4, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit4, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit4, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit4,0b00000000, 1); 
    }
  }
  if(digit == 5){
    if(num == 0){
      SetData(Digit5, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit5, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit5, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit5, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit5, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit5, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit5, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit5, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit5, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit5, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit5,0b00000000, 1); 
    }
  }
  if(digit == 6){
    if(num == 0){
      SetData(Digit6, 0b11111110, 1); 
    }
    if(num == 1){
      SetData(Digit6, 0b10110000, 1); 
    }
    if(num == 2){
      SetData(Digit6, 0b11101101, 1); 
    }
    if(num == 3){
      SetData(Digit6, 0b11111001, 1); 
    }
    if(num == 4){
      SetData(Digit6, 0b10110011, 1); 
    }
    if(num == 5){
      SetData(Digit6, 0b11011011, 1); 
    }
    if(num == 6){
      SetData(Digit6, 0b11011111, 1); 
    }
    if(num == 7){
      SetData(Digit6, 0b11110000, 1); 
    }
    if(num == 8){
      SetData(Digit6, 0b11111111, 1); 
    }
    if(num == 9){
      SetData(Digit6, 0b11111011, 1); 
    }
    if(num == 11){
      SetData(Digit6,0b00000000, 1); 
    }
  }
  if(digit == 7){
    if(num == 0){
      SetData(Digit7, 0b01111110, 1); 
    }
    if(num == 1){
      SetData(Digit7, 0b00110000, 1); 
    }
    if(num == 2){
      SetData(Digit7, 0b01101101, 1); 
    }
    if(num == 3){
      SetData(Digit7, 0b01111001, 1); 
    }
    if(num == 4){
      SetData(Digit7, 0b00110011, 1); 
    }
    if(num == 5){
      SetData(Digit7, 0b01011011, 1); 
    }
    if(num == 6){
      SetData(Digit7, 0b01011111, 1); 
    }
    if(num == 7){
      SetData(Digit7, 0b01110000, 1); 
    }
    if(num == 8){
      SetData(Digit7, 0b01111111, 1); 
    }
    if(num == 9){
      SetData(Digit7, 0b01111011, 1); 
    }
    if(num == 11){
      SetData(Digit7,0b00000000, 1); 
    }
  }
}



